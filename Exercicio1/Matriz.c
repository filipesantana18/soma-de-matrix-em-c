#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
#include <time.h>
#define NUM 3000

void MultiplicacaoMatriz(long int MatrizA[][NUM], long int MatrizB[][NUM], long int MatrizC[][NUM]){
	for (int i = 0; i < NUM; i++)
	{
		int valorTotal = 0;
		//linha
		for (int j = 0; j < NUM; j++)
		{			//coluna
			for (int k = 0; k < NUM; k++)
			{
				valorTotal += MatrizA[i][k] * MatrizB[k][i];
				//aqui cria a matriz da multiplica��o com i e j
			}
			MatrizC[i][j] = valorTotal;
		}

	}

}

void Popular(long int Matriz[][5]){

	for (int i = 0; i < NUM; i++)
	{		
		//linha
		for (int j = 0; j < NUM; j++)
		{
			Matriz[i][j] = i + j + 1;
	}
}
}

void mostrarMatriz(long int Matriz[][5]){

	for (int i = 0; i < NUM; i++)
	{
		//linha
		for (int j = 0; j < NUM; j++)
		{
			printf(" %d ",Matriz[i][j]);
		}
		printf("\n");
	}
}
int main(){

	static long int MatrizA[NUM][NUM];
	static long int MatrizB[NUM][NUM];
	static long int MatricC[NUM][NUM];

Popular(&MatrizA);
Popular(&MatrizB);

clock_t begin, end;
double time_spent;
begin = clock();

MultiplicacaoMatriz(&MatrizA, &MatrizB, &MatricC);

//verifica��o de tempo at� aqui
end = clock();
time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
printf("Tempo de finaliza��o: %f \n", time_spent);

//mostrarMatriz(MatricC);
getchar();
return(0);
}